<?php 
class Imap 
{
	public $imap = false;
	public $folder = 'Inbox';

	/**
	 * @param string
	 * @param string
	 * @param string
	 * @param boolean
	 * @return void
	 */
	public function __construct($mailbox, $username, $password, $protocol = false) 
	{	
		if (isset($protocol)) {
			switch ($protocol) {
				case 'ssl': $ptl = '/imap/ssl/novalidate-cert'; break;
				case 'tsl': $ptl = '/imap/tsl/novalidate-cert'; break;
				default: $ptl = '';
			}	
		} else {
			$ptl = '';
		}

		$this->mailbox = '{' . $mailbox . $ptl . '}';
		// Open an IMAP stream to a mailbox 
		$this->imap = imap_open($this->mailbox, $username, $password);
	}

	// close connection
	public function __destruct() 
	{
		if ($this->imap !== false) {
			// Close an IMAP stream
			imap_close($this->imap);
		}
	}

	/**
	 * @return boolean true if success
	 * return true after successfull connection
	 */
	public function isConnected()
	{
		return $this->imap !== false;
	}

	/**
	 * @return string error message
	 * return last imap error
	 */
	public function getError()
	{
		// Gets the last IMAP error that occurred during this page request
		return imap_last_error();
	}

	/**
	 * @param  string folder name
	 * @return boolean true if success open folder
	 * select folder
	 */
	public function selectFolder($folder)
	{
		// Reopen IMAP stream to new mailbox
		$result = imap_reopen($this->imap, $this->mailbox . $folder);

		if ($result === true) {
			$this->folder = $folder;
		}

		return $result;
	}

	/**
	 * @return array with foldernames
	 * return all folders
	 */
	public function listFolders()
	{
		// Read the list of mailboxes
		$folders = imap_list($this->imap, $this->mailbox, '*');
		return str_replace($this->mailbox, '', $folders);
	}

	/**
	 * @return int number of messages
	 * return number of messages in current mailbox
	 */
	public function numMessages()
	{
		// Gets the number of messages in the current mailbox
		return imap_num_msg($this->imap);
	}

	/**
	 * @param  info by imap
	 * @return string format 'Name <email@host.domain>'
	 * formation string addresser
	 */
	public function formatAddress($headerinfo)
	{
		if (isset($headerinfo->mailbox) && isset($headerinfo->host)) {
			$email = $headerinfo->mailbox . '@' . $headerinfo->host;
		} else {
			$email = '';
		}

		if (!empty($headerinfo->personal)) {
			// Decode MIME header elements
			$name = imap_mime_header_decode($headerinfo->personal);
			$name = $name[0]->text;
		} else {
			$name = $email;
		}

		// Converts MIME-encoded text to UTF-8
		$name = imap_utf8($name);

		return $name . ' <' . $email . '>';
	}

	/**
	 * @param  imap stream
	 * @param  message id
	 * @param  mimetype
	 * @param  boolean
	 * @param  boolean
	 * @return string email body
	 * get body email
	 */
	private function partMsg($imap, $uid, $mimetype, $structure = false, $part = false) {
        if (!$structure) {
        		// Read the structure of a particular message
               $structure = imap_fetchstructure($imap, $uid, FT_UID);
        }

        if ($structure) {
            if ($mimetype == $this->mimeTypes($structure)) {
                if (!$part) {
                    $part = 1;
                }

                // Fetch a particular section of the body of the message
                $text = imap_fetchbody($imap, $uid, $part, FT_UID | FT_PEEK);

                switch ($structure->encoding) {
                	// Decode BASE64 encoded text && remove html , php tags , and space
                    case 3: return imap_base64(trim(strip_tags($text)));
                    // Convert a quoted-printable string to an 8 bit string
                    case 4: return imap_qprint(trim(strip_tags($text)));
                    default: return trim(strip_tags($text));
               }
           }
     
            // for multipart 
            if ($structure->type == 1) {
                foreach ($structure->parts as $key => $subStruct) {
                    if ($part) {
                        $prefix = $part . ".";
                    } else {
                    	$prefix = '';
                    }

                    $data = $this->partMsg($imap, $uid, $mimetype, $subStruct, $prefix . ($key + 1));

                    if ($data) {
                        return $data;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * @param  $structure
     * @return string mime type
     * return mime type
     */
    private function mimeTypes($structure) {
        $mimeTypes = array('TEXT', 'MULTIPART', 'MESSAGE', 'APPLICATION', 'AUDIO', 'IMAGE', 'VIDEO', 'OTHER');
     
        if ($structure->subtype) {
           return $mimeTypes[(int)$structure->type] . "/" . $structure->subtype;
        }
        return 'TEXT/PLAIN';
    }

    /**
     * @param  int id message
     * @return array
     * get email body by id
     */
	private function getEmailBody($uid)
	{
		$body = $this->partMsg($this->imap, $uid, 'TEXT/HTML');
		$html = true;

		if ($body == '') {
			$body = $this->partMsg($this->imap, $uid, 'TEXT/PLAIN');
			$html = false;
		}

		// Converts MIME-encoded text to UTF-8
		$body = imap_utf8($body);

		return array('body' => $body, 'html' => $html);
	}

	/**
	 * @param  int message id
	 * @param  boolean
	 * @return array message
	 * return email in this format by id
	 */
	private function formatMessage($id, $withbody = true)
	{	
		// Read the header of the message
		$header = imap_headerinfo($this->imap, $id);
		// This function returns the UID for the given message sequence number
		$uid = imap_uid($this->imap, $id);

		// get data about email
		if (isset($header->subject) && strlen($header->subject) > 0) {
			foreach (imap_mime_header_decode($header->subject) as $obj) {
				$subject .= $obj->text;
			}
		} else {
			$subject = '';
		}

		// Converts MIME-encoded text to UTF-8
		$subject = imap_utf8($subject);

		$email = [
				'from'		=> $this->formatAddress($header->from[0]),
				'date'		=> date('d.m.Y', strtotime($header->MailDate)),
				'subject'	=> $subject,
				'uid'		=> $uid,
			];

		// get body email
		if ($withbody === true) {
			$body = $this->getEmailBody($uid);
			$email['body'] = $body['body'];
			$email['html'] = $body['html'];
		}

		return $email;

	}

	/**
	 * @param  boolean 
	 * @return array_key_exists(key, array)
	 * return all messages ( emails ) in current folder
	 */
	public function getMessages($withbody = true)
	{
		$numMessages = $this->numMessages();
		$messages = [];

		for ($i=1; $i <= $numMessages; $i++) { 
		 	$messages[] = $this->formatMessage($i, $withbody);
		 } 

		 return $messages;
	}

	/**
	 * @param  int id
	 * @param  boolean
	 * @return array
	 * return email by id
	 */
	public function getMessage($id, $withbody = true)
	{
		return $this->formatMessage($id, $withbody);
	}

	/**
	 * @param  int id
	 * @return bool
	 * delete message
	 */
	public function deleteMessage($id)
	{
		// Mark a message for deletion from current mailbox
		imap_delete($this->imap, $id, FT_UID);
		// Delete all messages marked for deletion
		return imap_expunge($this->imap);
	}

	/**
	 * @param  int id
	 * @return bool
	 * move to trash
	 */
	public function moveToTrash($id)
	{
		// Move specified messages to a mailbox
		imap_mail_move($this->imap, $id, '[Gmail]/Trash', FT_UID);
		// Move all messages marked for move
		return imap_expunge($this->imap);
	}

	/**
	 * @param  int uid
	 * @return int id
	 * get id by uid
	 */
	public function getId($uid)
	{
		// Gets the message sequence number for the given UID
		return imap_msgno($this->imap, $uid);
	}

	/*
	public function xmlFormatMessages()
	{
		$messages = $this->getMessages();
		$xml = new SimpleXMLElement('<message/>');
		array_walk_recursive($messages, array($xml, 'addChild'));
		return $xml->asXML();
	}
	*/
}