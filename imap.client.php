<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json'); 
require_once 'imap.class.php';

$imap = new Imap('imap.gmail.com', 'email', 'password', 'ssl');

if (isset($_GET['m'])) {
	print json_encode($imap->getMessage($imap->getId($_GET['m'])));
	
} elseif (isset($_GET['r'])) {
	$imap->moveToTrash($_GET['r']);
}	else {
		switch ($_GET['a']) {
			case 'spam':
				$imap->selectFolder('[Gmail]/Spam');
				print json_encode($imap->getMessages());
				break;

			case 'trash':
				$imap->selectFolder('[Gmail]/Trash');
				print json_encode($imap->getMessages());
				break;
			
			default:
				$imap->selectFolder('Inbox');
				print json_encode($imap->getMessages());
				break;
		}
}

/*
$folders = $imap->listFolders();
foreach($folders as $folder)
    echo $folder . '<br>';
*/

//echo $imap->numMessages();

//$imap->moveToTrash(6);